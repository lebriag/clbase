/*
 * This class contains the basic operations to be performed on a MongoDB
 * The type of DB used by CL, can be replaced by reimplementing this class 
 * and pointing the CLResourceDBConnector to the new class DBConnector.
 */
package eu.cloudlightning.clmongodbconnector;

import com.mongodb.BasicDBObject;
import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author Gabriel Gonzalez Castane
 */
public class CLMongoDBConnector {
    
         public static final String CLRESOURCEID ="_id";
         
        protected String BASE_URI;
        protected String dbProtocol;
        protected String dbHost;
        protected String dbPort;
        protected String dbName;   
        protected String dbUser;
        protected String dbPwd;        
        protected MongoClient client;
        protected MongoDatabase database;
        protected MongoCollection<Document> collection;       
        
    public CLMongoDBConnector(String user, String pwd, String host, String port, String name, String protocol) {
            this.dbProtocol = protocol;
            this.dbHost = host;
            this.dbName = name;
            this.dbPort = port;
//            this.dbUser = "";
//            this.dbPwd = "";
//          this.BASE_URI = this.dbProtocol + this.dbHost + ":" + this.dbPort;

        	this.dbUser = user;
        	this.dbPwd = pwd;            
            this.BASE_URI = this.dbProtocol + this.dbUser + ":" + this.dbPwd + "@" + this.dbHost + ":" + this.dbPort;                     
    }
    
    public String accessElement(String clResourceId)
    {
    	 JSONObject dbObj = null;
        
        try
        {
        	
            BasicDBObject query = new BasicDBObject(CLRESOURCEID, clResourceId);
            String dbObjStr = collection.find(query).first().toJson();
            // Remove object_id to force mongo db to provide the original event
            JSONParser parser = new JSONParser();
            dbObj = (JSONObject) parser.parse(dbObjStr);
            Object dbkey = "_id";
            dbObj.remove(dbkey, clResourceId);
        }
        catch (Exception e)
        {
            System.out.println("ERROR finding element with clResourceId["+ clResourceId + "] : \n");
            e.printStackTrace();
        }

        return dbObj.toString();  
    }
    
 
    
    public boolean recordElement(JSONObject value){
       boolean operationSuccess = true;
       try
        {  
            Document doc;      
            doc = new Document( (Map<String, Object>) JSON.parse( value.toString() ));
            this.collection.insertOne(doc);
            System.out.println("document: " + value);
        }
        catch (Exception e)
        {
            operationSuccess = false;
            System.out.println("ERROR recording ["+ value.toString() + "] : " + e.toString());
        }
       return operationSuccess;
    }

    public boolean deleteElement(String clResourceId){
        boolean operationSuccess = true;
       
        try
        {
        	if (accessElement(clResourceId) != null){
	            Document query = new Document(CLRESOURCEID, clResourceId);
	            this.collection.deleteOne(query);
	            System.out.println(query.toString());
        	} else {
        		operationSuccess = false;
        	}
        }
        catch (Exception e)
        {
            operationSuccess = false;
            System.out.println("ERROR recording clResource["+ clResourceId + "] : " + e.toString());
        }

       return operationSuccess;

    }
    
    public void finalize() {
        disconnect();
    }
    
    public void establishConnection(String dbTableName) {
    	client = new MongoClient(new MongoClientURI(this.BASE_URI));
        database = client.getDatabase(dbName);
        collection = database.getCollection(dbTableName);
    }
    
    public void changeDbTable(String dbTableName){
        collection= database.getCollection(dbTableName);
    }
    
    protected void disconnect() {
        client.close();
    }
    
    protected void printAllTableNames() {

        try (MongoCursor<String> collections = database.listCollectionNames().iterator()) {
            while (collections.hasNext()) {
                 System.out.println(collections.next());
            }
        }
    }
    
    protected void printAllElementsFromTable() {

        try (MongoCursor<Document> cursor = collection.find().iterator()) {
            while (cursor.hasNext()) {
                 System.out.println(cursor.next());
            }
        }
    }    
}