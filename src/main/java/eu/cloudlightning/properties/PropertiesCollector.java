/*
 * This class collects all the properties and set the environment variable within the properties file to run the project 
 */
package eu.cloudlightning.properties;

import eu.cloudlightning.properties.PropertiesConstants;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

/**
 *
 * @author gabriel
 */
public class PropertiesCollector extends PropertiesConstants {
    
    Properties properties = new Properties();
    
    public PropertiesCollector(){
        
	InputStream input = null;

	try {

		input = new FileInputStream(PropertiesConstants.CONFIGURATION);

		// load a properties file
		properties.load(input);
                
                
                String property;
                
                // Set properties for a Data Base
                if (properties.containsKey(PropertiesConstants.DBHOST_KEY)){
                    property = properties.getProperty(PropertiesConstants.DBHOST_KEY);
                    if (!property.isEmpty()) PropertiesConstants.setDBHOST_VALUE(property);
                    else 
                        throw new IOException("The IP Address at DBHOST is not setted at config.properties");

                    property = properties.getProperty(PropertiesConstants.DBPROTOCOL_KEY);
                    if (!property.isEmpty()) PropertiesConstants.setDBPROTOCOL_VALUE(property);
                    else 
                        throw new IOException("The protocol for the DB is not setted at config.properties");

                    property = properties.getProperty(PropertiesConstants.DBPORT_KEY);
                    if (!property.isEmpty()) PropertiesConstants.setDBPORT_VALUE(property);
                    else 
                        throw new IOException("The port for the DB is not setted at config.properties");

                    property = properties.getProperty(PropertiesConstants.DBNAME_KEY);
                    if (!property.isEmpty()) PropertiesConstants.setDBNAME_VALUE(property);
                    else 
                        throw new IOException("The name of the DB is not setted at config.properties");

                    property = properties.getProperty(PropertiesConstants.DBUSERAUTH_KEY);
                    if (!property.isEmpty()) PropertiesConstants.setDBUSERAUTH_VALUE(property);
                    else 
                        throw new IOException("The user for the DB is not setted at config.properties");

                    property = properties.getProperty(PropertiesConstants.DBPASSWORDAUTH_KEY);
                    if (!property.isEmpty()) PropertiesConstants.setDBPASSWORDAUTH_VALUE(property);
                    else 
                        throw new IOException("The password auth for the user DB is not setted at config.properties");
                }
                
                // Set properties for a Restful server
                if (properties.containsKey(PropertiesConstants.REST_PROTOCOL_KEY)){
                    // Set environment variables for the rest server
                    property = properties.getProperty(PropertiesConstants.REST_PROTOCOL_KEY);
                    if (!property.isEmpty()){
                        PropertiesConstants.setREST_PROTOCOL_VALUE(property);

                    }
                    else throw new IOException("The name of the DB is not setted at config.properties");

                    // Rest server
                    Map env = new Hashtable();

                    property = properties.getProperty(PropertiesConstants.REQUEST_TIMEOUT_KEY);
                    if (!property.isEmpty()) {
                        PropertiesConstants.setREQUEST_TIMEOUT_VALUE(property);                    
                    }
                    else
                        throw new IOException("Rest PATH for the REST server is not setted at configuration properties. Using 60 seconds as default.");
                   

                    property = properties.getProperty(PropertiesConstants.REST_HOSTNAME_KEY);
                    if (!property.isEmpty()) {
                        PropertiesConstants.setREST_HOSTNAME_VALUE(property);
                        env.put(PropertiesConstants.REST_HOSTNAME_KEY, property);
                    }
                    else 
                        throw new IOException("Hostname for the REST server is not setted at configuration properties. Using localhost by default.");

                    property = properties.getProperty(PropertiesConstants.REST_PORT_KEY);
                    if (!property.isEmpty()) {
                        PropertiesConstants.setREST_PORT_VALUE(property);
                        env.put(PropertiesConstants.REST_PORT_KEY, property);
                    }
                    else 
                        throw new IOException("Hostname for the REST server is not setted at configuration properties. Using localhost by default.");

                    property = properties.getProperty(PropertiesConstants.REST_PACKAGE_KEY);
                    if (!property.isEmpty()) {
                        PropertiesConstants.setREST_PACKAGE_VALUE(property);
                        env.put(PropertiesConstants.REST_PACKAGE_KEY, property);
                    }
                    else 
                        throw new IOException("Rest Package for the REST server is not setted at configuration properties. Using localhost by default.");

                    property = properties.getProperty(PropertiesConstants.REST_PATH_KEY);
                    if (!property.isEmpty()) {
                        PropertiesConstants.setREST_PATH_VALUE(property);
                        env.put(PropertiesConstants.REST_PATH_KEY, property);
                    }
                    else 
                        throw new IOException("Rest PATH for the REST server is not setted at configuration properties. Using \"/\" as default.");
                    
                    setEnv(env);
                }
                
                property = properties.getProperty(PropertiesConstants.COMPONENTID_KEY);
                if (!property.isEmpty()){
                    PropertiesConstants.setCOMPONENTID_VALUE(property);
                }
                else throw new IOException("It is required a component ID name and it is not setted at config.properties");
                
                // Set properties for a RabbitMQ FORWARDER
                 property = properties.getProperty(PropertiesConstants.RABBITMQ_HOST_KEY_FORWARD);
                 if (properties.containsKey(PropertiesConstants.RABBITMQ_HOST_KEY_FORWARD)){

                    // Set environment variables for the rest server
                    property = properties.getProperty(PropertiesConstants.RABBITMQ_HOST_KEY_FORWARD);
                    if (!property.isEmpty()){
                        PropertiesConstants.setRABBITMQ_HOST_VALUE_FORWARD(property);

                    }
                    else throw new IOException("The name of the host for the rabbitmq is not setted at config.properties");
                    
                    property = properties.getProperty(PropertiesConstants.RABBITMQ_UID_KEY_FORWARD);
                    if (!property.isEmpty()){
                        PropertiesConstants.setRABBITMQ_UID_VALUE_FORWARD(property);

                    }
                    else throw new IOException("The name of the uid for the rabbitmq is not setted at config.properties");
                    
                    property = properties.getProperty(PropertiesConstants.RABBITMQ_PWD_KEY_FORWARD);
                    if (!property.isEmpty()){
                        PropertiesConstants.setRABBITMQ_PWD_VALUE_FORWARD(property);

                    }
                    else throw new IOException("The password for the rabbitmq is not setted at config.properties");
                    
                    property = properties.getProperty(PropertiesConstants.RABBITMQ_QNAME_KEY_FORWARD);
                    if (!property.isEmpty()){
                        PropertiesConstants.setRABBITMQ_QNAME_VALUE_FORWARD(property);

                    }
                    else throw new IOException("The name of the queue name for the rabbitmq is not setted at config.properties");
                    
                    
                 }

                 // Set properties for a RabbitMQ WORKER
                 if (properties.containsKey(PropertiesConstants.RABBITMQ_HOST_KEY_RECEPTION)){
                    // Set environment variables for the rest server
                    property = properties.getProperty(PropertiesConstants.RABBITMQ_HOST_KEY_RECEPTION);
                    if (!property.isEmpty()){
                        PropertiesConstants.setRABBITMQ_HOST_VALUE_RECEPTION(property);

                    }
                    else throw new IOException("The name of the host for the rabbitmq is not setted at config.properties");
                    
                    property = properties.getProperty(PropertiesConstants.RABBITMQ_UID_KEY_RECEPTION);
                    if (!property.isEmpty()){
                        PropertiesConstants.setRABBITMQ_UID_VALUE_RECEPTION(property);

                    }
                    else throw new IOException("The name of the uid for the rabbitmq is not setted at config.properties");
                    
                    property = properties.getProperty(PropertiesConstants.RABBITMQ_PWD_KEY_RECEPTION);
                    if (!property.isEmpty()){
                        PropertiesConstants.setRABBITMQ_PWD_VALUE_RECEPTION(property);

                    }
                    else throw new IOException("The password for the rabbitmq is not setted at config.properties");
                    
                    property = properties.getProperty(PropertiesConstants.RABBITMQ_QNAME_KEY_RECEPTION);
                    if (!property.isEmpty()){
                        PropertiesConstants.setRABBITMQ_QNAME_VALUE_RECEPTION(property);

                    }
                    else throw new IOException("The name of the queue name for the rabbitmq is not setted at config.properties");
                    
                 }
                
	} catch (IOException ex) {
		ex.printStackTrace();
	} finally {
		if (input != null) {
			try {
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
    }
    
    public Properties getProperties() {
        return properties;
    }
    
    protected static void setEnv(Map<String, String> newenv)
    {
      try
        {
            Class<?> processEnvironmentClass = Class.forName("java.lang.ProcessEnvironment");
            Field theEnvironmentField = processEnvironmentClass.getDeclaredField("theEnvironment");
            theEnvironmentField.setAccessible(true);
            Map<String, String> env = (Map<String, String>) theEnvironmentField.get(null);
            env.putAll(newenv);
            Field theCaseInsensitiveEnvironmentField = processEnvironmentClass.getDeclaredField("theCaseInsensitiveEnvironment");
            theCaseInsensitiveEnvironmentField.setAccessible(true);
            Map<String, String> cienv = (Map<String, String>) theCaseInsensitiveEnvironmentField.get(null);
            cienv.putAll(newenv);
        }
        catch (NoSuchFieldException e)
        {
          try {
            Class[] classes = Collections.class.getDeclaredClasses();
            Map<String, String> env = System.getenv();
            for(Class cl : classes) {
                if("java.util.Collections$UnmodifiableMap".equals(cl.getName())) {
                    Field field = cl.getDeclaredField("m");
                    field.setAccessible(true);
                    Object obj = field.get(env);
                    Map<String, String> map = (Map<String, String>) obj;
                    map.clear();
                    map.putAll(newenv);
                }
            }
          } catch (SecurityException | NoSuchFieldException | IllegalArgumentException | IllegalAccessException e2) {
              // TODO: Add specific exception
          }
        } catch (ClassNotFoundException | SecurityException | IllegalArgumentException | IllegalAccessException e1) {
            // TODO: Add specific exception
        } 
    }

}