/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cloudlightning.util;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeoutException;
import eu.cloudlightning.properties.PropertiesConstants;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gabriel G. Castane
 */
public class QueueMessageForwarder {

    private Channel channel;
    private Connection connection;
    private String RABBITMQ_HOST_FORWARD;
    private String RABBITMQ_UID_FORWARD;
    private String RABBITMQ_PWD_FORWARD;
    private String RABBITMQ_QNAME_FORWARD;

    public QueueMessageForwarder(Properties config) throws IOException, TimeoutException {
        int timer = 30; //30seconds
        long currentTime = System.currentTimeMillis();
        while (( (System.currentTimeMillis() / 1000) - (currentTime / 1000) ) <= timer) {
            try {
                RABBITMQ_HOST_FORWARD = (config.getProperty(PropertiesConstants.RABBITMQ_HOST_KEY_FORWARD));
                RABBITMQ_UID_FORWARD = (config.getProperty(PropertiesConstants.RABBITMQ_UID_KEY_FORWARD));
                RABBITMQ_PWD_FORWARD = (config.getProperty(PropertiesConstants.RABBITMQ_PWD_KEY_FORWARD));
                RABBITMQ_QNAME_FORWARD = (config.getProperty(PropertiesConstants.RABBITMQ_QNAME_KEY_FORWARD));

                ConnectionFactory factory = new ConnectionFactory();
                factory.setHost(RABBITMQ_HOST_FORWARD);
                factory.setUsername(RABBITMQ_UID_FORWARD);
                factory.setPassword(RABBITMQ_PWD_FORWARD);

                connection = factory.newConnection();
                channel = this.connection.createChannel();
                System.out.println("Connection with Communication Channel OK");
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Error");
            }
            
            if(channel != null) {
              break;
            }
        }
    }

    public void sendMetrics(String message) throws IOException, TimeoutException {
        if (channel == null){
            if (connection == null){
                ConnectionFactory factory = new ConnectionFactory();
                connection = factory.newConnection();
            }
            channel = this.connection.createChannel();
        }
        
        //channel.queueDeclare(RABBITMQ_QNAME, false, false, true, null);
        channel.basicPublish("", RABBITMQ_QNAME_FORWARD, null, message.getBytes("UTF-8"));
        System.out.println("Request sent to the CellManager '" + message + "'");
        // channel.close();
    }

    public void close() throws IOException {
        try {
            this.channel.close();
            this.connection.close();
        } catch (TimeoutException ex) {
            Logger.getLogger(QueueMessageForwarder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}